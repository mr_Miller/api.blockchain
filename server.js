var express = require('express');
var log = require('./src/config/log')(module);
var node = require('./src/node');
var app = express();

app.get('/', (req, res) => {
  res.json('Its work');
});

app.use('/node', node);

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

//ERROR 404
app.use((req, res) => {
  res.status(404);
  log.debug('Not found URL: %s', req.url);
  res.send({ error: 'Not found' });
  return;
});

//ERROR 500
app.use((err, req, res) => {
  res.status(err.status || 500);
  log.error('Internal error(%d): %s', res.statusCode, err.message);
  res.send({ error: err.message });
  return;
});

app.listen(5000, '127.0.0.1', () => {
  log.info('Start Server');
});
