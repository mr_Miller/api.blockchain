const Sequelize = require("sequelize");
const sequelize = require("../sequelize");
const Settings = sequelize.define('settings', {
  'params': {
    type: Sequelize.STRING
  },
  'value': {
    type: Sequelize.STRING
  },
}, { createdAt: false, updatedAt: false });

module.exports.Settings = Settings;