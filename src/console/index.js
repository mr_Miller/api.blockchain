const { Settings } = require("../models/Settings");
const { Animal } = require("../models/Settings");
const config = require('../config/config');
const ps = require("ps-node");
const Web3 = require("web3");
const web3 = new Web3(new Web3.providers.HttpProvider(config.httpProvider));

const waitFor = (ms) => new Promise(r => setTimeout(r, ms));

const updateBlockIndex = (item, index) => new Promise(resolve => {
  resolve(item.update({ 'value': index }));
});

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array)
  }
}

async function asyncForBlock(startBlck, endBlock, callback) {
  for (let index = startBlck; index <= endBlock; index++) {
    await callback(index)
  }
  process.exit();
}

function fromHex(hex) {
  if (hex) {
    hex = hex.slice(2);
    let s = ''
    for (let i = 0; i < hex.length; i += 2) {
      s += String.fromCharCode(parseInt(hex.substr(i, 2), 16))
    }
    return decodeURIComponent(escape(s));
  }
  return '';
}

//DONE
ps.lookup({
  command: 'node',
  psargs: 'ux'
}, function (err, resultList) {
  if (err) {
    throw new Error(err);
  }
  var countProcess = 0;
  resultList.forEach(function (process) {
    if (process) {
      if (process.arguments == config.pathProcess) countProcess++;
      // console.log('PID: %s, COMMAND: %s, ARGUMENTS: %s', 
      // process.pid, process.command, process.arguments);
    }
  });
  if (countProcess < 2) {
    const blockNumber = web3.eth.blockNumber - 6;
    Settings.findOne({ where: { 'params': 'block_number_indexed' } }).then((setting, err) => {
      if (err) throw err;
      let blockIndexNumber = 1;
      if (parseInt(setting.get('value')) > 0) blockIndexNumber = parseInt(setting.get('value')) + 1;
      asyncForBlock(blockIndexNumber, blockNumber, async (i) => {
        await waitFor(50);
        let blockInfo = web3.eth.getBlock(i);
        console.log('blockNumber #', i, blockInfo.transactions.length);
        if (blockInfo.transactions.length > 0) {
          await asyncForEach(blockInfo.transactions, async (value) => {
            let transactionInfo = web3.eth.getTransaction(value);
            let input = fromHex(transactionInfo.input);
            if (input !== '') {
              let data = JSON.parse(input);
              data.block_number = i;
              data.block_hash = blockInfo.hash;
              data.tx_hash = value;
              Animal.create(data);
            }
          });
        }
        await updateBlockIndex(setting, i);
      });
    });
    console.log('Done');
  } else {
    console.log('COUNT PROCESS > 2');
  }
});
