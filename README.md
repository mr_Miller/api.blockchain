# Create file src/config.js
```
var config = {
  'DB_HOST': 'localhost',
  'DB_USER': 'root',
  'DB_PASSWORD': '',
  'DB_NAME': 'animal_index',
  'httpProvider': 'http://localhost:8545',
  "pathProcess": "~/src/console/index.js"
}
module.exports = config;
```