var express = require('express');
var fs = require("fs");
var router = express.Router();
var bodyParser = require("body-parser");
var crypto = require('crypto');
var config = require('./config/config');
var Web3 = require('web3');
var web3 = new Web3(new Web3.providers.HttpProvider(config.httpProvider));

web3._extend({
  methods: [new web3._extend.Method({
    name: 'propose',
    call: 'clique_propose',
    params: 2
  })]
});

var urlencodedParser = bodyParser.urlencoded({ extended: false });

router.post('/send-transaction', urlencodedParser, (req, res) => {
  if (!req.body) return res.sendStatus(400);
  if (!chechAccess(req.body.hash)) return res.sendStatus(401);
  web3.eth.sendTransaction({
    'from': '0x7d096cf73003d6d538e200184b3f8b15e1aecd39', // TODO add to config json
    'to': '0x082a9d3d75cb4410ec037c35820b130a2af3d099',
    // 'value': 10000,
    'data': web3.toHex(JSON.parse(req.body.data))
  }, (err) => {
    if (err) throw err;
    return res.json(true);
    // setTimeout(() => {
    //   return res.json(response);
    // }, 15000);
  });
});

router.post('/propose', urlencodedParser, (req, res) => {
  if (!req.body) return res.sendStatus(400);
  if (!chechAccess(req.body.hash)) return res.sendStatus(401);
  web3.propose(req.body.nodeHash, (req.body.auth == 1));
  return res.json(true);
});

/*FUNCTIONS*/
function chechAccess(hash) {
  var result = fs.readFileSync('src/config/auth_key.json', 'utf8', (error, data) => {
    if (error) throw error;
    var keys = JSON.parse(data);
    var decripted = createHash(keys.public_key, keys.private_key);
    if (decripted == hash) return true;
    return false;
  });
  return result;
}

function createHash(text, key) {
  return crypto.createHmac('sha256', key).update(text).digest('hex');
}

module.exports = router;