const Sequelize = require("sequelize");
const sequelize = require("../sequelize");
const Animal = sequelize.define('animals', {
  'block_number': {
    type: Sequelize.INTEGER
  },
  'block_hash': {
    type: Sequelize.STRING
  },
  'tx_hash': {
    type: Sequelize.STRING
  },
  'transponder': {
    type: Sequelize.STRING
  },
  'itisTSN': {
    type: Sequelize.INTEGER
  },
  'breed': {
    type: Sequelize.STRING
  },
  'gender': {
    type: Sequelize.INTEGER
  },
  'tag': {
    type: Sequelize.STRING
  },
  'nickname': {
    type: Sequelize.STRING
  },
  'color': {
    type: Sequelize.STRING
  },
  'birthDate': {
    type: Sequelize.BIGINT
  },
  'sterilization': {
    type: Sequelize.BIGINT
  },
  'localDbOwnerId': {
    type: Sequelize.INTEGER
  },
});

module.exports.Animal = Animal;