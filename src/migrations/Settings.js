var { Settings } = require('../models/Settings');
Settings.sync({ force: true }).then(() => {
  return Settings.create({
    params: 'block_number_indexed',
    value: 1
  });
});